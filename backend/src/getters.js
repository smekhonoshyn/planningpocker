import store from "./store.js";
import cardsData from "./cards-data.js";
import {
    centers
} from "../../shared/table-data.js";
import {createSessionHandler} from "./session.js";
import COMMON_EVENTS from "../../shared/common-events.js";
import {findClient} from "./client.js";
import NAVIGATION_PATHS from '../../shared/navigation-paths.js';
import CLIENT_ROLES from "../../shared/client-roles.js";
import {io} from "./index.js";

const getters = {
    sessionStatus,
    cardsMap,
    availableCards,
    defaultDeck,
    [COMMON_EVENTS.IS_NAVIGATION_ALLOWED]: isNavigationAllowed
};

export async function runGetterHandler(socket, {uuid, getter, payload}) {
    const getterFn = getters[getter];

    if (!getterFn) {
        console.error(`getter ${getter} not found`);

        socket.emit(uuid, [`getter ${getter} not found`, null]);

        return;
    }

    const result = await getterFn(socket, payload);

    // console.log(`result from getter "${getter}" with payload ${JSON.stringify(payload)}:`, result);

    socket.emit(uuid, [null, result]);
}

export async function runActionHandler(socket, {uuid, action, payload}) {
    const actionFn = {
        // setupPathVisited,
        // startPathVisited,
        createSessionHandler,
        [COMMON_EVENTS.CLIENT_SET_ROLE]: setClientRole,
        [COMMON_EVENTS.CLIENT_JOIN_ROOM]: joinClientToRoom,
        [COMMON_EVENTS.INTERRUPT_GAME]: interruptGame
    }[action];

    if (!actionFn) {
        console.error(`action ${action} not found`);

        socket.emit(uuid, [`action ${action} not found`, null]);

        return;
    }

    const result = await actionFn(socket, payload);

    // console.log(`result from action "${action}" with payload ${JSON.stringify(payload)}:`, result);

    socket.emit(uuid, [null, result]);
}

// function setupPathVisited(socket) {
//     findClient(socket.id)?.leaveRooms();
// }

// function startPathVisited(socket) {
//     findClient(socket.id)?.leaveRooms();
// }

function sessionStatus(socket, {uuid}) {
    // console.log('sessionStatus', uuid, typeof uuid);

    return {
        isKnown: store.hasModule(uuid),
        isArchived: false,
        canJoinAsPlayer: store.hasModule(uuid) && store.state[uuid].players.length < centers.length
    };
}

function cardsMap() {
    return cardsData.cardsMap;
}

function availableCards() {
    return cardsData.availableCards;
}

function defaultDeck() {
    return cardsData.defaultDeck;
}

function isNavigationAllowed(socket, {toPath, fromPath, context}) {
    if (toPath === NAVIGATION_PATHS.PLAY_ROUTE_PATH) {
        if (![NAVIGATION_PATHS.START_ROUTE_PATH, NAVIGATION_PATHS.SETUP_ROUTE_PATH].includes(fromPath)) {
            return {
                redirect: {
                    path: NAVIGATION_PATHS.START_ROUTE_PATH,
                    query: {
                        uuid: context.uuid
                    }
                }
            };
        }
    }

    return {
        redirect: null
    };
}

async function setClientRole(socket, {role}) {
    const client = findClient(socket.id);

    if (!client) {
        return {
            redirect: {
                path: NAVIGATION_PATHS.ERROR_ROUTE_PATH,
                meta: {
                    validationContext: null,
                    requiredRole: CLIENT_ROLES.CLIENT_ROLE_NONE,
                    targetRoom: null,
                    properties: {
                        message: `client "${socket.id}" was not found in registry`
                    }
                }
            }
        };
    }

    await client.setRole(role);

    return {
        redirect: null
    };
}

async function joinClientToRoom(socket, {roomId}) {
    const client = findClient(socket.id);

    if (!client) {
        return {
            redirect: {
                path: NAVIGATION_PATHS.ERROR_ROUTE_PATH,
                meta: {
                    validationContext: null,
                    requiredRole: CLIENT_ROLES.CLIENT_ROLE_NONE,
                    targetRoom: null,
                    properties: {
                        message: `client "${socket.id}" was not found in registry`
                    }
                }
            }
        };
    }

    await client.joinRoom(roomId);

    return {
        redirect: null
    };
}

async function interruptGame(socket, {roomId}) {
    io.to(roomId).emit(COMMON_EVENTS.FORCE_REDIRECT, {
        path: NAVIGATION_PATHS.START_ROUTE_PATH
    });
}
