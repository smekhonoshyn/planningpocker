import express from 'express';
import http from 'http';
import path from 'path';
import {
    Server
} from "socket.io";
import COMMON_EVENTS from "../../shared/common-events.js";
import {
    createClient
} from "./client.js";

const app = express();
const server = http.createServer(app);

app.use(express.static(path.resolve(`${process.cwd()}/../dist`)));

app.get('/*', (req, res) => {
    // res.sendFile(`${process.cwd()}/public/index.html`);
    res.sendFile(path.resolve(`${process.cwd()}/../dist/index.html`));
});

server.listen(9001);

export const io = new Server(server);

io.on(COMMON_EVENTS.SOCKET_CONNECTION, createClient);
