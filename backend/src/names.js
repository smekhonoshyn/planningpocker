import {
    uniqueNamesGenerator,
    adjectives,
    colors,
    animals,
    starWars
} from 'unique-names-generator';

const config = {
    dictionaries: [adjectives, colors, [...animals, ...starWars]],
    separator: ' ',
    length: 3,
    style: 'capital'
};

export function generateName() {
    return uniqueNamesGenerator(config);
}
