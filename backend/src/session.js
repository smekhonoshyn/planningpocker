import {
    v4 as uuidv4
} from 'uuid';
import store from "./store.js";
import {
    generateName
} from "./names.js";
import COMMON_EVENTS from '../../shared/common-events.js';
import STORE_EVENTS from "../../shared/store-events.js";
import {
    io
} from './index.js';
import {
    centers
} from "../../shared/table-data.js";
import cardsData from "./cards-data.js";
import ROUND_STATUSES from '../../shared/round-statuses.js';
import GAME_STATUSES from '../../shared/game-statuses.js';
import STORE_CONSTANTS from '../../shared/store-constants.js';
import {
    getSocket,
    getSockets
} from "./socket.js";
import sleep from "../../shared/sleep.js";

store.subscribe(async (mutation, state) => {
    const [type, namespace] = mutation.type.split('/').reverse();

    if (type === 'setPlayers' && state[namespace].players.length === 0) {
        await sleep();

        store.unregisterModule(namespace);

        await store.dispatch(`${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/${STORE_CONSTANTS.UNREGISTER_MODULE}`, {gameId: namespace});

        console.log(`unregistered module ${namespace}`);
    }
});

export async function createSessionHandler(socket, {
    selectedCards
}) {
    const sessionId = uuidv4();

    store.registerModule(sessionId, {
        namespaced: true,
        state: {
            sessionId,
            rounds: [],
            players: [],
            cardsDeck: selectedCards,
            places: centers.map(([x, y]) => ({
                position: {
                    transform: `translate3d(${x}px, ${y}px, 0) translate3d(-50%, -50%, 0)`
                },
                playerId: null
            })),
            log: [],
            gameStatus: GAME_STATUSES.GAME_STATUS_PRE,
            activeRound: null
        },
        mutations: {
            setPlayers(state, players) {
                // console.log('run mutation setPlayers with payload: ', players);

                state.players = players;
            },
            updateLog(state, entry) {
                // console.log('run mutation updateLog with payload: ', entry);

                state.log.push({uuid: uuidv4(), ...entry});
            },
            setPlaces(state, places) {
                // console.log('run mutation setPlaces with payload: ', places);

                state.places = places;
            },
            setRounds(state, rounds) {
                // console.log('run mutation setRounds with payload: ', rounds);

                state.rounds = rounds;
            },
            setGameStatus(state, gameStatus) {
                // console.log('run mutation setGameStatus with payload: ', gameStatus);

                state.gameStatus = gameStatus;
            },
            setActiveRound(state, activeRound) {
                // console.log('run mutation setActiveRound with payload: ', activeRound);

                state.activeRound = activeRound;
            }
        },
        actions: {
            async [COMMON_EVENTS.ADD_CLIENT]({dispatch}, {clientId}) {
                await dispatch('addPlayer', {clientId});
            },
            async addPlayer({dispatch, state}, {clientId}) {
                // console.log('run action "addPlayer" with payload: ', {clientId});

                const playerName = generateName();
                const players = [
                    ...state.players,
                    {
                        id: clientId,
                        name: playerName
                    }
                ];
                const logEntry = {action: 'addPlayer', detail: {playerId: clientId, playerName}};
                const emptyPlace = state.places.find(({playerId}) => playerId === null);
                const emptyPlaceIndex = state.places.indexOf(emptyPlace);
                const places = [
                    ...state.places.slice(0, emptyPlaceIndex),
                    {
                        ...emptyPlace,
                        playerId: clientId
                    },
                    ...state.places.slice(emptyPlaceIndex + 1)
                ];

                const mutations = [];

                if (state.gameStatus === GAME_STATUSES.GAME_STATUS_IN) {
                    const activeRound = {
                        ...state.activeRound,
                        roundPlayers: [
                            ...state.activeRound.roundPlayers,
                            clientId
                        ]
                    };

                    mutations.push(['setActiveRound', activeRound]);
                }

                mutations.push(['setPlayers', players]);
                mutations.push(['updateLog', logEntry]);
                mutations.push(['setPlaces', places]);

                const beforeBroadcast = async () => (await getSocket(clientId)).join(state.sessionId);

                return dispatch('commitChanges', {
                    mutations,
                    beforeBroadcast
                });
            },
            async [COMMON_EVENTS.REMOVE_CLIENT]({dispatch}, {clientId}) {
                await dispatch('removePlayer', {clientId});

                io.to(clientId).emit(STORE_EVENTS.STORE_COMMIT, {type: `${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/resetState`});
            },
            async removePlayer({dispatch, state}, {clientId}) {
                // console.log('run action "removePlayer" with payload: ', {clientId});

                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const players = state.players.filter(({id}) => id !== clientId);
                const logEntry = {action: 'removePlayer', detail: {playerId: clientId, playerName}};
                const playerPlace = state.places.find(({playerId}) => playerId === clientId);
                const playerPlaceIndex = state.places.indexOf(playerPlace);
                const places = [
                    ...state.places.slice(0, playerPlaceIndex),
                    {
                        ...playerPlace,
                        playerId: null
                    },
                    ...state.places.slice(playerPlaceIndex + 1)
                ];

                const mutations = [
                    ['setPlayers', players],
                    ['updateLog', logEntry],
                    ['setPlaces', places]
                ];
                const beforeBroadcast = async () => (await getSocket(clientId)).leave(state.sessionId);

                return dispatch('commitChanges', {
                    mutations,
                    beforeBroadcast
                });
            },
            async renamePlayer({dispatch, state}, {clientId, playerName}) {
                // console.log('run action "renamePlayer" with payload: ', {clientId, playerName});

                const {name} = state.players.find(({id}) => id === clientId);
                const players = state.players.map((player) => {
                    if (player.id !== clientId) {
                        return player;
                    }

                    return {
                        ...player,
                        name: playerName
                    };
                });
                const logEntry = {action: 'renamePlayer', detail: {playerId: clientId, playerNameBefore: name, playerNameAfter: playerName}};

                const mutations = [
                    ['setPlayers', players],
                    ['updateLog', logEntry]
                ];

                return dispatch('commitChanges', {mutations});
            },
            async estimate({dispatch, state}, {clientId, cardAlias}) {
                // console.log('run action "estimate" with payload: ', {clientId, cardAlias});

                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const logEntry = {action: 'estimate', detail: {playerId: clientId, playerName}};
                const activeRound = {
                    ...state.activeRound,
                    roundVotes: [
                        ...state.activeRound.roundVotes,
                        {
                            playerId: clientId,
                            cardAlias
                        }
                    ]
                };

                const mutations = [
                    ['updateLog', logEntry],
                    ['setActiveRound', activeRound]
                ];

                return dispatch('commitChanges', {mutations});
            },
            async createRound({dispatch, state}, {clientId}) {
                // console.log('run action "createRound" with payload: ', {clientId});

                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const roundIndex = state.rounds.length;
                const logEntry = {action: 'createRound', detail: {playerId: clientId, playerName, roundIndex: roundIndex + 1}};
                const rounds = [
                    ...state.rounds,
                    {
                        roundId: uuidv4(),
                        roundTopic: '',
                        roundStatus: ROUND_STATUSES.ROUND_STATE_INITIAL,
                        roundVotes: null,
                        roundPlayers: null
                    }
                ];

                const mutations = [
                    ['setRounds', rounds],
                    ['updateLog', logEntry]
                ];

                return dispatch('commitChanges', {mutations});
            },
            async changeRoundTopic({dispatch, state}, {clientId, roundId, roundTopic}) {
                // console.log('run action "changeRoundTopic" with payload: ', {clientId, roundId, roundTopic});

                const round = state.rounds.find((round) => round.roundId === roundId);
                const roundIndex = state.rounds.indexOf(round);
                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const logEntry = {action: 'changeRoundTopic', detail: {playerId: clientId, playerName, roundIndex: roundIndex + 1, roundTopic}};
                const rounds = [
                    ...state.rounds.slice(0, roundIndex),
                    {
                        ...round,
                        roundTopic
                    },
                    ...state.rounds.slice(roundIndex + 1)
                ];

                const mutations = [];

                if (state.activeRound && state.activeRound.roundId === roundId) {
                    const activeRound = {
                        ...state.activeRound,
                        roundTopic
                    };

                    mutations.push(['setActiveRound', activeRound]);
                }

                mutations.push(['setRounds', rounds]);
                mutations.push(['updateLog', logEntry]);

                return dispatch('commitChanges', {mutations});
            },
            async startRound({dispatch, state}, {clientId, roundId}) {
                // console.log('run action "startRound" with payload: ', {clientId, roundId});

                const round = state.rounds.find((round) => round.roundId === roundId);
                const roundIndex = state.rounds.indexOf(round);
                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const logEntry = {action: 'startRound', detail: {playerId: clientId, playerName, roundIndex: roundIndex + 1}};
                const activeRound = {
                    roundId: round.roundId,
                    roundTopic: round.roundTopic,
                    roundVotes: [],
                    roundPlayers: state.players.map(({id}) => id)
                };
                const rounds = [
                    ...state.rounds.slice(0, roundIndex),
                    {
                        ...round,
                        roundStatus: ROUND_STATUSES.ROUND_STATE_STARTED
                    },
                    ...state.rounds.slice(roundIndex + 1)
                ];

                const mutations = [
                    ['setRounds', rounds],
                    ['setGameStatus', GAME_STATUSES.GAME_STATUS_IN],
                    ['setActiveRound', activeRound],
                    ['updateLog', logEntry]
                ]

                return dispatch('commitChanges', {mutations});
            },
            async endRound({dispatch, state}, {clientId, roundId}) {
                // console.log('run action "endRound" with payload: ', {clientId, roundId});

                const round = state.rounds.find((round) => round.roundId === roundId);
                const roundIndex = state.rounds.indexOf(round);
                const {name: playerName} = state.players.find(({id}) => id === clientId);
                const logEntry = {action: 'endRound', detail: {playerId: clientId, playerName, roundIndex: roundIndex + 1}};
                const rounds = [
                    ...state.rounds.slice(0, roundIndex),
                    {
                        ...round,
                        roundStatus: ROUND_STATUSES.ROUND_STATE_ENDED,
                        roundVotes: state.activeRound.roundVotes,
                        roundPlayers: state.activeRound.roundPlayers
                    },
                    ...state.rounds.slice(roundIndex + 1)
                ];

                const mutations = [
                    ['setRounds', rounds],
                    ['setGameStatus', GAME_STATUSES.GAME_STATUS_POST],
                    ['updateLog', logEntry]
                ];

                return dispatch('commitChanges', {mutations});
            },
            async commitChanges({commit, state}, {mutations = [], beforeBroadcast = async () => {}} = {}) {
                mutations.forEach(([type, payload]) => {
                    commit(type, payload);
                });

                await beforeBroadcast();

                await broadcastSessionData(state.sessionId);
            }
        },
        getters: {
            async mappedState(state) {
                return {
                    adminData: {
                        log: state.log,
                        players: state.players
                    },
                    commonData: {
                        sessionId: state.sessionId,
                        cardsDeck: state.cardsDeck,
                        rounds: state.rounds.map((round) => ({
                            roundId: round.roundId,
                            roundTopic: round.roundTopic,
                            wasStarted: [ROUND_STATUSES.ROUND_STATE_STARTED, ROUND_STATUSES.ROUND_STATE_ENDED].includes(round.roundStatus),
                            wasEnded: round.roundStatus === ROUND_STATUSES.ROUND_STATE_ENDED,
                            canBeStarted: state.gameStatus === GAME_STATUSES.GAME_STATUS_PRE
                                ? true
                                : state.gameStatus === GAME_STATUSES.GAME_STATUS_IN
                                    ? false
                                    : round.roundStatus === ROUND_STATUSES.ROUND_STATE_INITIAL,
                            canBeEnded: state.gameStatus === GAME_STATUSES.GAME_STATUS_PRE
                                ? false
                                : state.gameStatus === GAME_STATUSES.GAME_STATUS_IN
                                    ? round.roundId === state.activeRound.roundId
                                    : false,
                            statistics: round.roundVotes && round.roundVotes
                                .filter(({cardAlias}) => cardsData.cardsMap[cardAlias].isNumeric)
                                .reduce((accumulator, vote) => {
                                    const item = accumulator.find(({cardAlias}) => cardAlias === vote.cardAlias);

                                    if (!item) {
                                        accumulator.push({
                                            cardAlias: vote.cardAlias,
                                            votesCount: 0
                                        });
                                    }

                                    accumulator.find(({cardAlias}) => cardAlias === vote.cardAlias).votesCount++;

                                    return accumulator;
                                }, [])
                                .sort(({cardAlias: cardAliasA}, {cardAlias: cardAliasB}) => {
                                    return cardsData.cardsMap[cardAliasA].priority - cardsData.cardsMap[cardAliasB].priority;
                                }),
                            roundPlayersCount: round.roundPlayers && round.roundPlayers.length
                        })),
                        log: state.log
                    },
                    perPlayerData: (await getSockets(sessionId)).map(({id}) => id).reduce((accumulator, socketId) => {
                        return {
                            ...accumulator,
                            [socketId]: {
                                playerName: state.players.find(({id}) => id === socketId).name,
                                playerDeckDisabled: [GAME_STATUSES.GAME_STATUS_PRE, GAME_STATUSES.GAME_STATUS_POST].includes(state.gameStatus)
                                    || !state.activeRound
                                    || Boolean(state.activeRound.roundVotes.find(({playerId}) => playerId === socketId)),
                                places: state.places.map((place) => {
                                    const player = place.playerId && state.players.find(({id}) => id === place.playerId);
                                    const playerName = place.playerId && player.name;
                                    const vote = state.activeRound && state.activeRound.roundVotes.find(({playerId}) => playerId === place.playerId) || null;
                                    const cardAlias = vote && (
                                        state.gameStatus === GAME_STATUSES.GAME_STATUS_POST
                                            ? vote.cardAlias
                                            : place.playerId === socketId
                                                ? vote.cardAlias
                                                : 'svgBack'
                                    );

                                    return {
                                        position: place.position,
                                        playerId: place.playerId,
                                        playerName,
                                        currentPlayer: place.playerId === socketId,
                                        card: cardAlias && cardsData.cardsMap[cardAlias]
                                    };
                                }),
                                activeRound: state.activeRound && {
                                    roundId: state.activeRound.roundId,
                                    roundTopic: state.activeRound.roundTopic
                                }
                            }
                        };
                    }, {})
                };
            }
        },
        strict: process.env.NODE_ENV !== 'production'
    });

    await store.dispatch(`${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/${STORE_CONSTANTS.REGISTER_MODULE}`, {gameId: sessionId});

    console.log(`registered module ${sessionId}`);

    return {sessionId};
}

async function broadcastSessionData(sessionId) {
    const mappedState = await store.getters[`${sessionId}/mappedState`];

    if (!mappedState) {
        // error case

        return;
    }

    const {
        adminData,
        commonData,
        perPlayerData
    } = mappedState;

    io.to(sessionId).emit(STORE_EVENTS.STORE_COMMIT, {type: `${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/mappedState`, payload: commonData});

    Object.entries(perPlayerData).forEach(([socketId, playerData]) => {
        io.to(socketId).emit(STORE_EVENTS.STORE_COMMIT, {type: `${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/mappedState`, payload: playerData});
    });

    await store.dispatch(`${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/${STORE_CONSTANTS.UPDATE_GAME_DATA}`, {gameId: sessionId, gameData: adminData});
}
