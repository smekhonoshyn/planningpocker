class AbstractCard {
    static tag = null;

    get asPlainObject() {
        return {
            tag: this.tag,

            // @todo: move these to abstract check
            priority: this.priority,
            isNumeric: this.isNumeric
        };
    }

    get tag() {
        return this.constructor.tag;
    }

    get priority() {
        return 1024;
    }

    get isNumeric() {
        return false;
    }
}

class TextCard extends AbstractCard {
    static tag = 'span';

    constructor(text) {
        super();

        this.text = text;
    }

    get asPlainObject() {
        return {
            ...super.asPlainObject,
            text: this.text
        };
    }

    get priority() {
        return this.isNumeric ? +this.text : 256;
    }

    get isNumeric() {
        return !isNaN(this.text);
    }
}

class IconCard extends AbstractCard {
    static tag = 'base-icon';
    
    constructor(text, {inherit = true, outlined = false} = {}) {
        super();

        this.text = text;
        this.inherit = inherit;
        this.outlined = outlined;
    }

    get asPlainObject() {
        return {
            ...super.asPlainObject,
            text: this.text,
            modifiers: {
                inherit: this.inherit,
                outlined: this.outlined
            }
        };
    }

    get priority() {
        return 512;
    }
}

class SvgCard extends AbstractCard {
    constructor(tag) {
        super();

        this.ownTag = tag;
    }

    get tag() {
        return this.ownTag;
    }

    get priority() {
        return 512;
    }
}

const cardsMap = Object.entries({
    text0: new TextCard('0'),
    text0p5: new TextCard('0.5'),
    text1: new TextCard('1'),
    text2: new TextCard('2'),
    text3: new TextCard('3'),
    text5: new TextCard('5'),
    text8: new TextCard('8'),
    text13: new TextCard('13'),
    text20: new TextCard('20'),
    text21: new TextCard('21'),
    text40: new TextCard('40'),
    text100: new TextCard('100'),
    textQuestion: new TextCard('?'),
    iconCake: new IconCard('cake'),
    iconThumbUp: new IconCard('thumb_up'),
    iconThumbDown: new IconCard('thumb_down'),
    iconFavorite: new IconCard('favorite'),
    iconEmojiEmotions: new IconCard('emoji_emotions'),
    iconEuro: new IconCard('euro'),
    svgBack: new SvgCard('card-back'),
    svgTaco: new SvgCard('taco-card'),
    svgCoffee: new SvgCard('coffee-card')
}).reduce((accumulator, [key, value]) => ({...accumulator, [key]: value.asPlainObject}), {});

const availableCards = [
    'text0',
    'text0p5',
    'text1',
    'text2',
    'text3',
    'text5',
    'text8',
    'text13',
    'text20',
    'text21',
    'text40',
    'text100',
    'textQuestion',
    'iconCake',
    'svgCoffee',
    'iconThumbUp',
    'iconThumbDown',
    'iconFavorite',
    'iconEmojiEmotions',
    'iconEuro',
    'svgTaco'
];

const defaultDeck = [
    'text1',
    'text3',
    'text5',
    'text8',
    'text13',
    'text21',
    'textQuestion',
    'iconCake',
    'svgCoffee',
    'svgTaco'
];

export default {
    cardsMap,
    availableCards,
    defaultDeck
};
