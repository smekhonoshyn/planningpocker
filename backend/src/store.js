import Vue from 'vue';
import Vuex from 'vuex';
import STORE_CONSTANTS from '../../shared/store-constants.js';
import {
    getSocket
} from "./socket.js";
import COMMON_EVENTS from "../../shared/common-events.js";
import {io} from "./index.js";
import STORE_EVENTS from "../../shared/store-events.js";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    strict: process.env.NODE_ENV !== 'production',
    modules: {
        [STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME]: {
            namespaced: true,
            state: {
                gamesList: []
            },
            mutations: {
                setGamesList(state, gamesList) {
                    state.gamesList = gamesList;
                }
            },
            actions: {
                async [COMMON_EVENTS.ADD_CLIENT]({}, {clientId}) {
                    (await getSocket(clientId)).join(STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME);

                    broadcastSessionData();
                },
                async [COMMON_EVENTS.REMOVE_CLIENT]({}, {clientId}) {
                    (await getSocket(clientId)).leave(STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME);

                    io.to(clientId).emit(STORE_EVENTS.STORE_COMMIT, {type: `${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/resetState`});
                },
                [STORE_CONSTANTS.REGISTER_MODULE]({commit, state}, {gameId}) {
                    const gamesList = [
                        ...state.gamesList,
                        {
                            gameId,
                            gameData: {}
                        }
                    ];

                    commit('setGamesList', gamesList);

                    broadcastSessionData();
                },
                [STORE_CONSTANTS.UNREGISTER_MODULE]({commit, state}, {gameId}) {
                    const gamesList = state.gamesList.filter((gameItem) => gameItem.gameId !== gameId);

                    commit('setGamesList', gamesList);

                    broadcastSessionData();
                },
                [STORE_CONSTANTS.UPDATE_GAME_DATA]({commit, state}, {gameId, gameData}) {
                    const gamesList = state.gamesList.map((gameItem) => {
                        if (gameItem.gameId !== gameId) {
                            return gameItem;
                        }

                        return {
                            gameId,
                            gameData
                        };
                    });

                    commit('setGamesList', gamesList);

                    broadcastSessionData();
                }
            },
            getters: {},
            strict: process.env.NODE_ENV !== 'production',
            modules: {}
        }
    }
});

function broadcastSessionData() {
    const mappedState = store.state[STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME];

    if (!mappedState) {
        // error case

        return;
    }

    io.to(STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME).emit(STORE_EVENTS.STORE_COMMIT, {type: `${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/mappedState`, payload: mappedState});
}

export default store;
