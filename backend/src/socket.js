import {
  io
} from "./index.js";

export async function getSocket(clientId) {
  return (await io.in(clientId).fetchSockets())[0];
}

export function getSockets(sessionId) {
  return io.in(sessionId).fetchSockets();
}
