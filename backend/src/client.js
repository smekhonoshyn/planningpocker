import COMMON_EVENTS from "../../shared/common-events.js";
import STORE_EVENTS from "../../shared/store-events.js";
import store from "./store.js";
import {runActionHandler, runGetterHandler} from "./getters.js";
import CLIENT_ROLES from '../../shared/client-roles.js';

export function findClient(clientId) {
  return clientsRegistry.get(clientId);
}

export function createClient(socket) {
  new Client(socket);
}

const CLIENT_ROLES_PLAIN = [
  CLIENT_ROLES.CLIENT_ROLE_NONE,
  CLIENT_ROLES.CLIENT_ROLE_ADMIN,
  CLIENT_ROLES.CLIENT_ROLE_PLAYER
];

const clientsRegistry = new Map();
const clientsRegistryByRole = CLIENT_ROLES_PLAIN.reduce((accumulator, role) => Object.assign(accumulator, {[role]: new Map()}), {});

class Client {
  socket;

  constructor(socket) {
    console.log(`client ${socket.id} is connected`);

    this.socket = socket;

    clientsRegistry.set(this.id, this);

    this.setRole(CLIENT_ROLES.CLIENT_ROLE_NONE);

    this.setupListeners();
  }

  destroy() {
    // deregisterAs(this.role, this);

    clientsRegistry.delete(this.id);

    /* ??? */ this.socket.destroy?.();
  }

  get id() {
    return this.socket.id;
  }

  /**
   * @returns {String}
   */
  get role() {
    return CLIENT_ROLES_PLAIN.find((role) => clientsRegistryByRole[role].has(this.id));
  }

  /**
   * @param {String} newRole
   */
  setRole(newRole) {
    const currentRole = this.role;

    if (!currentRole) {
      registerAs(newRole, this);

      return;
    }

    if (currentRole === newRole) {
      return;
    }

    deregisterAs(currentRole, this);
    registerAs(newRole, this);

    return this.leaveRooms();
  }

  get rooms() {
    const {id} = this;

    return [...this.socket.rooms].filter((roomId) => roomId !== id);
  }

  joinRoom(roomId) {
    return this.dispatchToStore(`${roomId}/${COMMON_EVENTS.ADD_CLIENT}`);

    // this.socket.join(roomId);
  }

  leaveRoom(roomId) {
    return this.dispatchToStore(`${roomId}/${COMMON_EVENTS.REMOVE_CLIENT}`);

    // this.socket.leave(roomId);
  }

  leaveRooms() {
    return Promise.all(this.rooms.map((roomId) => this.leaveRoom(roomId)));
  }

  dispatchToStore(event, payload = {}) {
    return store.dispatch(event, {
      ...payload,
      clientId: this.id
    });
  }

  setupListeners() {
    this.socket.on(COMMON_EVENTS.SOCKET_DISCONNECTING, () => {
      console.log(`client ${this.id} is disconnecting`);
      console.log(`client ${this.id} rooms:`, JSON.stringify(this.rooms, null, 4));

      this.setRole(CLIENT_ROLES.CLIENT_ROLE_NONE);
    });

    this.socket.on(COMMON_EVENTS.SOCKET_DISCONNECT, (reason) => {
      console.log(`client ${this.id} was disconnected for the reason: ${reason}`);

      this.destroy();
    });

    this.socket.on(STORE_EVENTS.STORE_DISPATCH, ({type, payload}) => {
      return this.dispatchToStore(type, payload);
    });

    // this.socket.on(COMMON_EVENTS.CREATE_SESSION, createSessionHandler.bind(null, this.socket));

    this.socket.on(COMMON_EVENTS.GETTER_REQUEST_EVENT, runGetterHandler.bind(null, this.socket));

    this.socket.on(COMMON_EVENTS.ACTION_REQUEST_EVENT, runActionHandler.bind(null, this.socket));

    // this.socket.on(COMMON_EVENTS.CLIENT_SET_ROLE, ({role}) => {
    //   this.role = role;
    // });

    // this.socket.on(COMMON_EVENTS.CLIENT_JOIN_ROOM, ({roomId}) => {
    //   this.joinRoom(roomId);
    // });
  }
}

function registerAs(role, client) {
  clientsRegistryByRole[role].set(client.id, client);
}

function deregisterAs(role, client) {
  clientsRegistryByRole[role].delete(client.id);
}
