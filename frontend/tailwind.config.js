module.exports = {
  content: ["./src/**/*.{html,js,vue}"],
  theme: {
    extend: {
      // fontFamily: {
      //   "open-sans": "'Open Sans', sans-serif",
      // },
      fontSize: {
        'size-inherit': 'inherit'
      },
      fontWeight: {
        'weight-inherit': 'inherit'
      },
      height: {
        33: '8.25em'
      },
      minHeight: {
        37: '9.25em'
      },
      colors: {
        "gpc-primary": {
          50: "#e9ebf8",
          100: "#c8ccef",
          200: "#a4abe4",
          300: "#7f8ad9",
          400: "#636fd0",
          500: "#4855c7",
          600: "#424cbc",
          700: "#3942b0",
          800: "#3137a4",
          900: "#232490",
        },
        "gpc-accent": {
          50: "#D6F6F6",
          100: "#90EDEB",
          200: "#49E4DF",
          300: "#03DBD4",
          400: "#03CDD3",
          500: "#03C0D3",
          600: "#03B2D2",
          700: "#008EA5",
          800: "#007A8C",
          900: "#005861",
        },
        "gpc-red": {
          50: "#ffecef",
          100: "#ffced4",
          200: "#f69c9d",
          300: "#ef7476",
          400: "#fb5353",
          500: "#ff4037",
          600: "#f23737",
          700: "#e02c31",
          800: "#d32429",
          900: "#c4141c",
        },
        "gpc-green": {
          50: "#e4f7e8",
          100: "#beeac7",
          200: "#91dda2",
          300: "#5CD07C",
          400: "#22c55e",
          500: "#00ba3e",
          600: "#00aa35",
          700: "#009828",
          800: "#00871b",
          900: "#006703",
        },
        "gpc-yellow": {
          50: "#FFF9E1",
          100: "#FEEEB3",
          200: "#FEE381",
          300: "#FED94C",
          400: "#FECE20",
          500: "#FFC600",
          600: "#FFB800",
          700: "#FFA400",
          800: "#FFA400",
          900: "#FF7300",
        },
        "gpc-grey": {
          50: "#f8fafc",
          100: "#f1f5f9",
          200: "#e2e8f0",
          300: "#cbd5e1",
          400: "#94a3b8",
          500: "#64748b",
          600: "#475569",
          700: "#334155",
          800: "#1E293B",
          900: "#0F172A",
        }
      },
      cursor: {
        inherit: 'inherit'
      },
      boxShadow: {
        // "2dp":
        //   "0 1px 5px 0 rgb(0 0 0 / 20%), 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 12%)",
        // "4dp":
        //   "0 2px 4px -1px rgb(0 0 0 / 20%), 0 4px 5px 0 rgb(0 0 0 / 14%), 0 1px 10px 0 rgb(0 0 0 / 12%)",
        "button": "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
        "panel": "0 0 5px 0 #94a3b8"
      },
      maxWidth: {
        '3/4': '75%',
        '1/10': '10%',
        '8/10': '80%'
      },
      content: {
        'empty': '\'\''
      }
    },
  },
  plugins: [],
};
