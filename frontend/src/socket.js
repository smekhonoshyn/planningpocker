import {
    io
} from "socket.io-client";
import store from 'frontend/src/store';
import COMMON_EVENTS from 'shared/common-events';
import STORE_EVENTS from 'shared/store-events';
import STORE_CONSTANTS from "shared/store-constants";
import router from "frontend/src/router";

export const socket = io();

socket.on(STORE_EVENTS.STORE_COMMIT, ({type, payload}) => {
    // console.log(STORE_EVENTS.STORE_COMMIT, {type, payload});

    store.dispatch(type, payload);
});

socket.on(COMMON_EVENTS.FORCE_REDIRECT, (redirectParams) => {
    router.push(redirectParams);
});

// socket.on(COMMON_EVENTS.NAVIGATE_TO_GAME, (sessionId) => {
//     router.push({path: PLAY_ROUTE_PATH, query: {
//         uuid: sessionId
//     }});
// });

export function dispatch(type, payload) {
    dispatchWithSessionId(type, store.state[STORE_CONSTANTS.STORE_GAME_MODULE_NAME].sessionId, payload);
}

export function dispatchWithSessionId(type, sessionId, payload) {
    emit(STORE_EVENTS.STORE_DISPATCH, {
        type: `${sessionId}/${type}`,
        payload
    });
}

export const emit = socket.emit.bind(socket);
export const once = socket.once.bind(socket);
