import Vue from 'vue';
import {
  sync
} from 'vuex-router-sync';
import App from 'frontend/src/App.vue';
import router from 'frontend/src/router';
import i18n from 'frontend/src/i18n';
import store from 'frontend/src/store';
import 'frontend/src/socket';

sync(store, router);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  i18n,
  store
}).$mount('#app');
