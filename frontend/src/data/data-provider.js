import {
  emit,
  once
} from "frontend/src/socket";
import {
  v4 as uuidv4
} from 'uuid';
import COMMON_EVENTS from 'shared/common-events';

export function runGetter(getter, payload) {
  return new Promise((resolve) => {
    const uuid = uuidv4();

    once(uuid, ([error, result]) => {
      if (error) {
        console.error(error);

        resolve(null);

        return;
      }

      resolve(result);
    });

    emit(COMMON_EVENTS.GETTER_REQUEST_EVENT, {
      uuid,
      getter,
      payload
    });
  });
}

export function runAction(action, payload) {
  return new Promise((resolve) => {
    const uuid = uuidv4();

    once(uuid, ([error, result]) => {
      if (error) {
        console.error(error);

        resolve(null);

        return;
      }

      resolve(result);
    });

    emit(COMMON_EVENTS.ACTION_REQUEST_EVENT, {
      uuid,
      action,
      payload
    });
  });
}
