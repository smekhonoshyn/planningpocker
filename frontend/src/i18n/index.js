import Vue from 'vue';
import VueI18n from 'vue-i18n';

import enLocalization from 'frontend/src/i18n/en.json';

Vue.use(VueI18n);

const messages = {
    en: enLocalization
};

export default new VueI18n({
    locale: 'en',
    messages
});
