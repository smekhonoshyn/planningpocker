import Vue from 'vue';
import Router from 'vue-router';

import SetupGameScreen from 'frontend/src/views/SetupGameScreen';
import PlayerScreen from "frontend/src/views/PlayerScreen";
import StartGameScreen from "frontend/src/views/StartGameScreen";
import AdminScreen from "frontend/src/views/AdminScreen";
import ErrorScreen from "frontend/src/views/ErrorScreen";

import {
    runAction,
    runGetter
} from "frontend/src/data/data-provider";
import COMMON_EVENTS from "shared/common-events";
import NAVIGATION_PATHS from 'shared/navigation-paths';
import CLIENT_ROLES from "shared/client-roles";
import STORE_CONSTANTS from "shared/store-constants";

const {
    CLIENT_ROLE_NONE,
    CLIENT_ROLE_PLAYER,
    CLIENT_ROLE_ADMIN
} = CLIENT_ROLES;

Vue.use(Router);

const routes = [
    {
        path: NAVIGATION_PATHS.ROOT_ROUTE_PATH,
        redirect: NAVIGATION_PATHS.START_ROUTE_PATH
    },
    {
        path: NAVIGATION_PATHS.START_ROUTE_PATH,
        component: StartGameScreen,
        props: (route) => ({
            uuid: route.query.uuid
        }),
        meta: {
            validationContext: (to) => ({
                uuid: to.query.uuid
            }),
            requiredRole: CLIENT_ROLE_NONE,
            targetRoom: null,
            properties: {}
        }
    },
    {
        path: NAVIGATION_PATHS.SETUP_ROUTE_PATH,
        component: SetupGameScreen,
        meta: {
            validationContext: null,
            requiredRole: CLIENT_ROLE_NONE,
            targetRoom: null,
            properties: {}
        }
    },
    {
        path: NAVIGATION_PATHS.PLAY_ROUTE_PATH,
        component: PlayerScreen,
        props: (route) => ({
            uuid: route.query.uuid
        }),
        meta: {
            validationContext: (to) => ({
                uuid: to.query.uuid
            }),
            requiredRole: CLIENT_ROLE_PLAYER,
            targetRoom: (to) => ({
                roomId: to.query.uuid
            }),
            properties: {}
        }
    },
    {
        path: NAVIGATION_PATHS.ADMIN_ROUTE_PATH,
        component: AdminScreen,
        meta: {
            validationContext: null,
            requiredRole: CLIENT_ROLE_ADMIN,
            targetRoom: {
                roomId: STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME
            },
            properties: {}
        }
    },
    {
        path: NAVIGATION_PATHS.ERROR_ROUTE_PATH,
        component: ErrorScreen,
        props: (route) => ({
            message: route.meta.properties.message
        }),
        meta: {
            validationContext: null,
            requiredRole: CLIENT_ROLE_NONE,
            targetRoom: null,
            properties: {}
        }
    }
];

const router = new Router({
    mode: 'history',
    routes
});

router.beforeEach(async (to, from, next) => {
    // console.log({
    //     isFreshRun: from.fullPath === '/' && !from.redirectedFrom,
    //     fromPath: from.path,
    //     toPath: to.path
    // });

    const {
        redirect: validationRedirect
    } = await runGetter(COMMON_EVENTS.IS_NAVIGATION_ALLOWED, getValidationContextPayload(to, from));

    if (validationRedirect) {
        return next(validationRedirect);
    }

    if (shouldResetCurrentRole(to, from)) {
        await runAction(COMMON_EVENTS.CLIENT_SET_ROLE, getNoneRolePayload());
    }

    if (shouldSetRequiredRole(to, from)) {
        const {
            redirect: requiredRoleRedirect
        } = await runAction(COMMON_EVENTS.CLIENT_SET_ROLE, getRequiredRolePayload(to, from));

        if (requiredRoleRedirect) {
            return next(requiredRoleRedirect);
        }
    }

    if (to.meta.targetRoom) {
        const {
            redirect: targetRoomRedirect
        } = await runAction(COMMON_EVENTS.CLIENT_JOIN_ROOM, getTargetRoomPayload(to, from));

        if (targetRoomRedirect) {
            return next(targetRoomRedirect);
        }
    }

    next();
});

function shouldResetCurrentRole(to, from) {
    if (from.meta.requiredRole === CLIENT_ROLE_PLAYER && to.meta.requiredRole === CLIENT_ROLE_PLAYER) {
        return true;
    }

    return false;
}

function shouldSetRequiredRole(to, from) {
    if (from.meta.requiredRole === CLIENT_ROLE_NONE && to.meta.requiredRole === CLIENT_ROLE_NONE) {
        return false;
    }

    if (from.meta.requiredRole === CLIENT_ROLE_ADMIN && to.meta.requiredRole === CLIENT_ROLE_ADMIN) {
        return false;
    }

    return true;
}

function getValidationContextPayload(to, from) {
    return {
        toPath: to.path,
        fromPath: from.path,
        context: typeof to.meta.validationContext === 'function'
          ? to.meta.validationContext(to, from)
          : to.meta.validationContext
    };
}

function getNoneRolePayload() {
    return {
        role: CLIENT_ROLE_NONE
    };
}

function getRequiredRolePayload(to) {
    return {
        role: to.meta.requiredRole
    };
}

function getTargetRoomPayload(to, from) {
    return typeof to.meta.targetRoom === 'function'
        ? to.meta.targetRoom(to, from)
        : to.meta.targetRoom;
}

export default router;
