import Vue from 'vue';
import Vuex from 'vuex';
import STORE_CONSTANTS from "shared/store-constants";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    strict: process.env.NODE_ENV !== 'production',
    modules: {
        [STORE_CONSTANTS.STORE_GAME_MODULE_NAME]: {
            namespaced: true,
            state: getDefaultGameState(),
            mutations: {
                mappedState(state, mappedState) {
                    // console.log(`mutation "${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/mappedState"`);

                    Object.assign(state, mappedState);
                },
                resetState(state) {
                    // console.log(`mutation "${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/resetState"`);

                    Object.assign(state, getDefaultAdministrationState());
                }
            },
            actions: {
                mappedState({commit}, mappedState) {
                    // console.log(`action "${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/mappedState"`);

                    commit('mappedState', mappedState);
                },
                resetState({commit}) {
                    // console.log(`action "${STORE_CONSTANTS.STORE_GAME_MODULE_NAME}/resetState"`);

                    commit('resetState');
                }
            },
            getters: {},
            strict: process.env.NODE_ENV !== 'production',
            modules: {}
        },
        [STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME]: {
            namespaced: true,
            state: getDefaultAdministrationState(),
            mutations: {
                resetState(state) {
                    // console.log(`mutation "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/resetState"`);

                    Object.assign(state, getDefaultAdministrationState());
                },
                setGamesList(state, gamesList) {
                    // console.log(`mutation "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/setGamesList"`);

                    state.gamesList = gamesList;
                },
                setSelectedGame(state, selectedGameId) {
                    // console.log(`mutation "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/setSelectedGame"`);

                    state.selectedGameId = state.gamesList.find(({gameId}) => gameId === selectedGameId)
                        ? selectedGameId
                        : null;
                }
            },
            actions: {
                mappedState({commit, state}, {gamesList}) {
                    // console.log(`action "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/mappedState"`);

                    commit('setGamesList', gamesList);
                    commit('setSelectedGame', state.selectedGameId);
                },
                resetState({commit}) {
                    // console.log(`action "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/resetState"`);

                    commit('resetState');
                },
                setSelectedGame({commit}, selectedGameId) {
                    // console.log(`action "${STORE_CONSTANTS.STORE_ADMINISTRATION_MODULE_NAME}/setSelectedGame"`);

                    commit('setSelectedGame', selectedGameId);
                }
            },
            getters: {},
            strict: process.env.NODE_ENV !== 'production',
            modules: {}
        }
    }
});

function getDefaultGameState() {
    return {
        sessionId: null,
        cardsDeck: [],
        rounds: [],
        log: [],

        places: [],
        playerName: null,
        activeRound: null,
        playerDeckDisabled: true
    };
}

function getDefaultAdministrationState() {
    return {
        gamesList: [],
        selectedGameId: null
    };
}
