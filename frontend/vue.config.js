const path = require('path');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Planning Poker';

        return args;
      });

    config
      .resolve
      .alias
      .set('shared', path.resolve('../shared'))
      .set('frontend', path.resolve('.'));

    // config
    //   .plugin('webpack-bundle-analyzer')
    //   .use(BundleAnalyzerPlugin)
    //   .init(Plugin => new Plugin({
    //     openAnalyzer: true
    //   }));

    // config
    //   .externals({ vue: 'Vue' });
  },
  outputDir: path.resolve('../dist')
};
