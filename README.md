# planning-poker (MVP)

### Project clean
```
./clean.sh
```

### Project setup
```
./setup.sh
```

### Project build
```
./build.sh
```

### Project run
```
./run.sh
```

### Project full run on clean env
```
./setup.sh && ./build.sh && ./run.sh
```

### Run details
- server port - **9001**