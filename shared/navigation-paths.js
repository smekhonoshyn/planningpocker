export default {
  ROOT_ROUTE_PATH: '/',
  START_ROUTE_PATH: '/start',
  SETUP_ROUTE_PATH: '/setup',
  PLAY_ROUTE_PATH: '/play',
  ADMIN_ROUTE_PATH: '/admin',
  ERROR_ROUTE_PATH: '/error'
};
