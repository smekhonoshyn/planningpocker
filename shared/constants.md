# Common Events #
- _CREATE_SESSION_
- <!-- _NAVIGATE_TO_GAME_ -->
- _SOCKET_CONNECTION_
- _SOCKET_DISCONNECT_
- _SOCKET_CONNECT_
- _GETTER_REQUEST_EVENT_
- _ACTION_REQUEST_EVENT_
  
# Game Statuses #
- _GAME_STATUS_PRE_: player deck is disabled; no active round; no started rounds; no ended rounds; any initial round can be started; on initial round start move to state "in-game"
- _GAME_STATUS_IN_: player deck is not disabled; active round; no rounds can be started; active round can be ended; on active round end move to state "post-game"
- _GAME_STATUS_POST_: player deck is disabled; active round; any initial round can be started; on initial round start move to state "in-game"

## player-related actions
- addPlayer:
  - if _GAME_STATUS_PRE_ - do nothing
  - if _GAME_STATUS_IN_ - add player to **roundPlayers**
  - if _GAME_STATUS_POST_ - do nothing
- removePlayer:
  - if _GAME_STATUS_PRE_ - do nothing
  - if _GAME_STATUS_IN_ - do nothing
  - if _GAME_STATUS_POST_ - do nothing
- startRound: create **roundPlayers** from existing players
- endRound: copy **roundPlayers** to round

# Round Statuses #
- _ROUND_STATE_INITIAL_
- _ROUND_STATE_STARTED_
- _ROUND_STATE_ENDED_

# Store Events #
- _STORE_COMMIT_
- _STORE_DISPATCH_