export default {
  STORE_COMMIT: 'commit',
  STORE_DISPATCH: 'dispatch'
};
