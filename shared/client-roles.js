export default {
  CLIENT_ROLE_NONE: 'NONE',
  CLIENT_ROLE_ADMIN: 'ADMIN',
  CLIENT_ROLE_PLAYER: 'PLAYER'
};
