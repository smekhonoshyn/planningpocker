const R = 120;
const D = 2 * R * Math.sin(Math.PI / 3);

export const centers = [
  [1 * R + 0 * D, 2 * R],
  [3 * R + 2 * D, 4 * R],
  [1 * R + 0 * D, 4 * R],
  [3 * R + 2 * D, 2 * R],
  [1 * R + 1 * D, 5 * R],
  [3 * R + 1 * D, 1 * R],
  [3 * R + 1 * D, 5 * R],
  [1 * R + 1 * D, 1 * R]
];
export const tablePath = `M${R + D} ${2 * R} h${2 * R} a ${R} ${R} 0 0 1 0 ${2 * R} h-${2 * R} a ${R} ${R} 0 0 1 0 -${2 * R}`;
export const tableSizes = [4 * R, 2 * R];
export const tableViewBox = [4 * R + 2 * D, 6 * R];
