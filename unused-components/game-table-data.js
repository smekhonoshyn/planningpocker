const angleDegShift = 60;
const initialAngleDeg = 0;
const sideLength = 160;
const initialVector = [sideLength, 0];

const angles = new Array(6)
    .fill(0)
    .map((none, index) => angleDegShift * index + initialAngleDeg)
    .map((angleDeg) => angleDeg * Math.PI / 180);

const vertices = angles.map((angleRad) => rotate(initialVector, angleRad));

const vectors = [
    rotate(initialVector, angleDegShift * 2 * Math.PI / 180),
    rotate(initialVector, angleDegShift * 3 * Math.PI / 180),
    rotate(initialVector, angleDegShift * 4 * Math.PI / 180),
    [0, -sideLength],
    [0, -sideLength],
    rotate(initialVector, angleDegShift * 5 * Math.PI / 180),
    rotate(initialVector, angleDegShift * 0 * Math.PI / 180),
    rotate(initialVector, angleDegShift * 1 * Math.PI / 180),
    [0, sideLength]
];

const tableVertices = [
    ...[
        vertices[2],
        vertices[3],
        vertices[4]
    ].map((points) => shift(points, [-sideLength, 0])),

    [...vertices[4]],
    [...vertices[5]],

    ...[
        vertices[5],
        vertices[0],
        vertices[1]
    ].map((points) => shift(points, [sideLength, 0])),

    [0, vertices[1][1]]
];//.map((point, index) => shift(point, vectors[index]));

const placeVertices = tableVertices.map(([x, y]) => [Number(x.toFixed(2)), Number(y.toFixed(2))]);

const [
    placeVerticesMinX,
    placeVerticesMinY
] = getMinBoundary(placeVertices);

const placeVertices3 = placeVertices
    .map((point) => shift(point, [-placeVerticesMinX, -placeVerticesMinY]));

export const placesPositions = placeVertices3.map(([x, y]) => [Number(x.toFixed(2)), Number(y.toFixed(2))]);

// console.log(JSON.stringify(placesPositions.map(([x, y], index) => ({x, y, index})), null, 4));

export const tableSpaceSizes = getTableSpaceSizes(placesPositions);
export const tableSpacePath = getTableSpacePath(placesPositions);

function getMinBoundary(points) {
    const pointsXs = points.map(([x]) => x);
    const pointsYs = points.map(([x, y]) => y);

    return [Math.min(...pointsXs), Math.min(...pointsYs)];
}

function getMaxBoundary(points) {
    const pointsXs = points.map(([x]) => x);
    const pointsYs = points.map(([x, y]) => y);

    return [Math.max(...pointsXs), Math.max(...pointsYs)];
}

function getTableSpaceSizes(placesPositions) {
    return [
        Math.max(...placesPositions.map(([x]) => x)) * 220/160,
        Math.max(...placesPositions.map(([x, y]) => y)) * 220/160
    ];
}

function getTableSpacePath([firstPosition, ...restPositions]) {
    return `
        M${firstPosition[0]} ${firstPosition[1]}
        ${restPositions.map((position) => `L${position[0]} ${position[1]}`).join(' ')}
        Z
    `;
}

function getTablePath(placesPositions) {
    const [
        placePositionsMaxX,
        placePositionsMaxY
    ] = getMaxBoundary(placesPositions);

    const firstBasePoint = [sideLength, placePositionsMaxY / 2];
    const secondBasePoint = [placePositionsMaxX - sideLength, placePositionsMaxY / 2];

    return '';
}

function rotate([x, y], angleRad) {
    const sin = Math.sin(angleRad);
    const cos = Math.cos(angleRad);

    return [
        x * cos - y * sin,
        x * sin + y * cos
    ];
}

function shift([x, y], [sx, sy]) {
    return [
        x + sx,
        y + sy
    ];
}

function scale([x, y], [xf, yf]) {
    return [
        x * xf,
        y * yf
    ];
}

function midPoint([fx, fy], [tx, ty]) {
    return [
        (fx + tx) / 2,
        (fy + ty) / 2
    ];
}
