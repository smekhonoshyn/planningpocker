import placesPositions from "@/data/places-positions.json";

function getRandomPlaces() {
    return placesPositions.map(([x, y]) => ({
        position: {
            transform: `translate3d(${x}px, ${y}px, 0) translate3d(-50%, -50%, 0)`
        },
        card: getRandomCard()
    }));
}

function getRandomCard() {
    const randomCardsGetters = [
        getRandomNumericCard,
        getRandomIconCard,
        () => null
    ];

    return randomCardsGetters[Math.floor(Math.random() * randomCardsGetters.length)]();
}

const numericCards = [1, 2, 3, 5, 8, 13, 21, 100, '?'];
const iconCards = ['cake', 'coffee'];

function getRandomNumericCard() {
    return {
        tag: 'span',
        text: numericCards[Math.floor(Math.random() * numericCards.length)]
    };
}

function getRandomIconCard() {
    return {
        tag: 'base-icon',
        text: iconCards[Math.floor(Math.random() * iconCards.length)],
        modifiers: {
            inherit: true
        }
    };
}
